package bdd;

import java.sql.Connection;
import java.sql.DriverManager;

public class MyConnection {
	public static Connection getConnection() {

		Connection con = null;
		try {
		//	Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bdd?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "root", "root");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return con;

	}

	public static void main(String[] args) {
		MyConnection.getConnection();
		// MyConnection my= new MyConnection();

	}
}
